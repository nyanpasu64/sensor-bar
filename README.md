# Sensor Bar Slim

A single-PCB USB-powered Wii sensor bar replacement, designed to be mounted flush against the bezel of small (eg. 17") VGA CRT monitors (rather than TVs), and used in relatively close range.

![Photo of Sensor Bar Slim v1 installed on a VGA CRT, with power wires routed straight up](images/photo.jpg)

*Photo of v1 PCB installed on a VGA CRT.*

This sensor bar is shorter than a regular Wii sensor bar, so it can be used closer to the display without the LEDs exiting the Wiimote's field of view. And the LEDs in each cluster are closer together, so the cursor doesn't jitter when close to the sensor bar.

- The v1 PCB was 99mm wide for discount ordering; v2 was extended to a width of 119mm, which works better at long distances.
    - In my testing, v1 would not work well across a room, at distances of around 5-6 feet or more. It is difficult to make a sensor bar work well both close-up (a wide sensor bar will exceed the Wiimote's field of view), and at long distances (a narrow sensor bar has difficulty being tracked accurately by the sensor, or being recognized as a sensor bar by Wii games outside of homebrew).
    - Note that even with a short sensor bar that fits fully in the Wiimote's field of view, the Wii cursor will not appear on-screen if the Wiimote is tilted too far up or down. This reduces the advantages of the shorter length.

Another notable difference from standard sensor bars is that the LEDs point out the *face* of the PCB. This allows it to be mounted on the bezel of a VGA CRT for more accurate vertical pointing, rather than being placed on top of the CRT's thick bezels.

I've mounted the PCB to my CRT bezel (just above the screen) using double-sided tape (torn in half lengthwise to match the PCB's width), then soldered on a long USB-to-pigtail cable.

## Ordering

I ordered from LCSC and JLCPCB; you can order the parts and board from other vendors if desired.

LCSC parts:

```csv
LCSC Part Number,Manufacture Part Number,Manufacturer,Package,Customer #,Description,RoHS,Order Qty.,Min\Mult Order Qty.,Unit Price,Order Price,Product Link
C965892,XL-3216IRC-940,XINGLIGHT,1206,,120° -30℃~+85℃ 5V 30mA 70mW 1.2V~1.6V 939nm~946nm 1206  Infrared (IR) LEDs ROHS,yes,20,20,0.0360,0.72,https://assets.lcsc.com/images/lcsc/96x96/20230131_XINGLIGHT-XL-3216IRC-940_C965892_front.jpg
C23182,0603WAF470JT5E,UNI-ROYAL(Uniroyal Elec),0603,,100mW Thick Film Resistors 75V ±100ppm/℃ ±1% -55℃~+155℃ 47Ω 0603  Chip Resistor - Surface Mount ROHS,yes,100,100,0.0010,0.10,https://assets.lcsc.com/images/lcsc/96x96/20221229_UNI-ROYAL-Uniroyal-Elec-0603WAF470JT5E_C23182_front.jpg
```

- LEDs are polarized; the tops of my LEDs have a green dot on the negative terminal, and the bottoms have a triangle pointing from positive to negative terminal (like the diode symbol but without the line).
- You can actually pick a slightly lower resistance than 47 ohms to drive them at their rated current of 30 mA, since I based my calculation off a 1.2 volt LED voltage drop, and in practice they had 1.27 to 1.3 volts of voltage drop.

JLCPCB ordering:

- [Export Gerber files from KiCad project](https://support.jlcpcb.com/article/194-how-to-generate-gerber-and-drill-files-in-kicad-6).
    - You need to fill zones during export; I do not fill zones before committing to Git because it pollutes diffs.
- [Upload to JLCPCB](https://cart.jlcpcb.com/quote), wait for processing.
- Pick a solder mask color (I picked white to blend into a beige monitor).
- Pick "PCB Thickness" (I picked 1.2mm, you could go thinner as well).
- If desired, click "LeadFree HASL" to avoid lead exposure (this will add $1.10).
- Under "Remove Order Number", click "Specify a location".

## Thoughts about V2

- This PCB is so easy to assemble, you could solder it drunk.
- On a white PCB, you can install SMD resistors upside-down to blend in better, since the bottom face is white.

There are some changes i'd make in a V2:

- [ ] Smaller LED footprint pads, for smaller solder blobs that don't visually stick out from the solder mask and monitor bezel. (Might be less drunk soldering friendly?)
    - We don't need hand solder footprints (which have longer pads), because the LEDs' pins have notches which expose the pads underneath.
- [x] Stagger power pads horizontally, so if you route power wires straight up from the PCB, they won't overlap.
    - It may be more robust to route the wires rightwards, then tie them down to the PCB before directing them up the monitor. But this is uglier with thick wires.
- [ ] Pick bigger holes for power pads, or switch to surface mount pads.
    - V1's power holes were too small for my thick power wires to fit through. So I flooded the holes with solder to surface-mount the power wires on top, and accidentally let solder come out the bottom of the hole, making the sensor bar harder to mount flush until I cleaned up the excess solder.
- [x] Use smaller (or less?) stitching vias. Even when I set JLCPCB to "Via Covering: Tented", many of V1's vias weren't covered by solder mask, appearing as black pinholes on a white background.
- [x] Pick a longer length (around 120mm wide?) for better performance at television distances.
    - This will raise JLCPCB costs from $2 to $4.30 (both not including shipping), which is still quite affordable.
